# fastDT

#### 项目介绍
分布式事物解决方案：基于消息队列和数据库的最终一致性

#### 软件架构
软件架构说明


#### 安装教程

1. 实现了分布式事物的最终一致性（基于消息队列和数据库mysql）
2. 参考：https://mp.weixin.qq.com/s/7u5zfrLzk38tDwOfDEkuqw
3. 本例子中，消息队列使用rabbitmq，数据库使用mysql。经过多轮测试并且已经在企业中运用。可在生成环境中使用

#### 使用说明

1. 先扣库存（库存服务），再扣用户钱（用户服务），最后生成订单（订单服务）
2. 发送端消息表：
tbl_dt_msg
id name business_val queue_val data status_val retry_num field1 field2 field3 create_time update_time 

status_val：
1：预发送
2：发送成功
3：发送失败
retry_num：最多5次，超过的话人工介入



3. 接收端防重表
tbl_dt_juge_duplicate

id msg_id msg_data handle_num status_val create_time update_time

msg_id（拿到的消息id）结合status_val判重

status_val：
1：预处理
2：处理成功
3：处理失败

handle_num：最多3次，人工介入

