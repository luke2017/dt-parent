package com.luke.dt.commons.entitys.user;

import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * 生成serialVersionUID
 * 参考：https://blog.csdn.net/qq_35246620/article/details/77686098
 */
@Data
@Table(name = "tbl_user")
public class TblUser implements Serializable {

    private static final long serialVersionUID = 5104753872420820987L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "balance")
    private Float balance;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "update_time")
    private Date updateTime;

}
