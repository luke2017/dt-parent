package com.luke.dt.commons.input.stock;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
public class BuyGoodsReqDto implements Serializable {

    private static final long serialVersionUID = 3323514916602819501L;

    @NotNull
    private String name;

    @NotNull
    private String merchanName;

    @NotNull
    private Integer count;

}
