package com.luke.dt.commons.inter.stock;

import com.luke.dt.commons.input.stock.FindStockReq;
import com.luke.dt.commons.output.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(value = "dt-stock")
@RequestMapping("/dt-stock/stock")
public interface StockInterController {

    @GetMapping("/findStock")
    Result findStock(FindStockReq findStockReq);

}
