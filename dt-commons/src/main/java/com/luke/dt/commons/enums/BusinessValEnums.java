package com.luke.dt.commons.enums;

import lombok.Getter;

@Getter
public enum BusinessValEnums {

    BUSINESS_VAL_REDU_USER_BALANCE(1001,"扣减用户余额");

    private Integer code;

    private String message;

    BusinessValEnums(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

}
