package com.luke.dt.commons.input.stock;

import lombok.Data;
import javax.validation.constraints.NotNull;

@Data
public class FindStockReq {

    @NotNull
    private String merchan;

}
