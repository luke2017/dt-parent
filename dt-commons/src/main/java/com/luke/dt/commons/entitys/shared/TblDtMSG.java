package com.luke.dt.commons.entitys.shared;

import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * 分布式事物发送端消息表
 */
@Data
@Table(name="tbl_dt_msg")
public class TblDtMSG implements Serializable {

    private static final long serialVersionUID = 171731975587712874L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "business_val")
    private Integer businessVal;

    @Column(name = "queue_val")
    private String queueVal;

    @Column(name = "data")
    private String data;

    @Column(name = "status_val")
    private Integer statusVal;

    @Column(name = "retry_num")
    private Integer retryNum;

    @Column(name = "field1")
    private String field1;

    @Column(name = "field2")
    private String field2;

    @Column(name = "field3")
    private String field3;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "update_time")
    private Date updateTime;

}
