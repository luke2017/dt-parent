package com.luke.dt.commons.output;

import lombok.Data;

@Data
public class Result {

    private Integer code;

    private String message;

    private Object data;

    public Result() {
    }

    public Result(Integer code, String message, Object data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public Result(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

}
