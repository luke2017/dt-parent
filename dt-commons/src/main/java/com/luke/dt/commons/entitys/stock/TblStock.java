package com.luke.dt.commons.entitys.stock;

import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Data
@Table(name="tbl_stock")
public class TblStock implements Serializable {

    private static final long serialVersionUID = 364205297888650091L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "merchan_name")
    private String merchanName;

    @Column(name = "merchan_price")
    private Float merchanPrice;

    @Column(name = "stock")
    private Integer stock;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "update_time")
    private Date updateTime;

}
