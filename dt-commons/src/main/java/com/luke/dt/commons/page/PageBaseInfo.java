package com.luke.dt.commons.page;

import lombok.Data;

@Data
public class PageBaseInfo {

    private Integer pageNum;

    private Integer pageSize;

}
