package com.luke.dt.commons.inter.user;

import com.luke.dt.commons.input.user.FindUserReq;
import com.luke.dt.commons.output.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(value = "dt-user")
@RequestMapping("/dt-user/user")
public interface UserInterController {

    @GetMapping(value = "/findUser")
    Result findUser(FindUserReq findUserReq);

}
