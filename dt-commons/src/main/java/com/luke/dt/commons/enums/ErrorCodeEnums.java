package com.luke.dt.commons.enums;

import lombok.Getter;

@Getter
public enum  ErrorCodeEnums {

    ERROR_CODE_SUCCESS(0,"成功"),

    ERROR_CODE_NOT_USER(-1,"用户不存在"),

    ERROR_CODE_INVALID_PARAMS(-2,"参数不完整或者错误"),

    ERROR_CODE_SERVICE_CALL_ERROR_STOCK(-3,"库存服务调用失败"),

    ERROR_CODE_SERVICE_CALL_ERROR_ORDER(-4,"订单服务调用失败"),

    ERROR_CODE_SERVICE_CALL_ERROR_USER(-5,"用户服务调用失败"),

    ERROR_CODE_SERVICE_NOT_FIND_STOCK(-6,"找不到库存"),

    ERROR_CODE_STOCK_NOT_ENOUGH(-7,"库存不够用"),

    ERROR_CODE_USER_BALANCE_NOT_ENOUGH(-8,"用户余额不足"),

    ERROR_CODE_SERVICE_NOT_FIND_USER(-9,"找不到用户"),

    ERROR_CODE_STOCK_REDU_FAIL(-10,"扣减库存失败"),

    ERROR_CODE_JUDE_FIND_FAIL(-11,"找不到防重记录"),

    ERROR_CODE_ERROR_SERVER(-100,"服务异常");

    private Integer code;

    private String message;

    ErrorCodeEnums(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

}
