package com.luke.dt.commons.entitys.shared;

import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * 分布式事物接收端防止重复处理消息表
 */
@Data
@Table(name="tbl_dt_juge_duplicate")
public class TblDtJudeDuplicate implements Serializable {

    private static final long serialVersionUID = 3372622256623543491L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "msg_id")
    private Long msgId;

    @Column(name = "msg_data")
    private String msgData;

    @Column(name = "handle_num")
    private Integer handleNum;

    @Column(name = "status_val")
    private Integer statusVal;

    @Column(name = "create_time")
    private Date createTime;

    @Column(name = "update_time")
    private Date updateTime;

}
