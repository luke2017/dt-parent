package com.luke.dt.commons.input.user;

import lombok.Data;
import javax.validation.constraints.NotNull;

@Data
public class FindUserReq {

    @NotNull
    private String name;

}
