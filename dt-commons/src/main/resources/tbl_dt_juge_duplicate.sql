/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50724
 Source Host           : localhost:3306
 Source Schema         : dt_user

 Target Server Type    : MySQL
 Target Server Version : 50724
 File Encoding         : 65001

 Date: 19/04/2019 16:31:18
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tbl_dt_juge_duplicate
-- ----------------------------
DROP TABLE IF EXISTS `tbl_dt_juge_duplicate`;
CREATE TABLE `tbl_dt_juge_duplicate`  (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `msg_id` bigint(11) NOT NULL COMMENT '消息id',
  `msg_data` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '消息内容',
  `handle_num` int(1) NOT NULL COMMENT '处理次数（最多三次，之后不再处理，需人工介入处理）',
  `status_val` int(1) NOT NULL COMMENT '处理状态 1：预处理 2：处理成功 3：处理失败',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_time` datetime(0) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbl_dt_juge_duplicate
-- ----------------------------
INSERT INTO `tbl_dt_juge_duplicate` VALUES (1, 7, '{\"balance\":7.2,\"id\":1}', 4, 2, '2019-04-19 11:37:35', '2019-04-19 14:23:22');
INSERT INTO `tbl_dt_juge_duplicate` VALUES (2, 8, '{\"balance\":7.2,\"id\":1}', 0, 2, '2019-04-19 14:25:46', '2019-04-19 14:26:00');
INSERT INTO `tbl_dt_juge_duplicate` VALUES (3, 9, '{\"balance\":13116.35,\"id\":2}', 0, 2, '2019-04-19 14:34:30', '2019-04-19 14:34:30');
INSERT INTO `tbl_dt_juge_duplicate` VALUES (4, 10, '{\"balance\":13116.35,\"id\":2}', 0, 2, '2019-04-19 14:41:30', '2019-04-19 14:41:30');
INSERT INTO `tbl_dt_juge_duplicate` VALUES (5, 11, '{\"balance\":13116.35,\"id\":2}', 0, 2, '2019-04-19 14:47:26', '2019-04-19 14:47:26');
INSERT INTO `tbl_dt_juge_duplicate` VALUES (6, 12, '{\"balance\":13116.35,\"id\":2}', 0, 2, '2019-04-19 14:51:51', '2019-04-19 14:51:51');
INSERT INTO `tbl_dt_juge_duplicate` VALUES (7, 13, '{\"balance\":13116.35,\"id\":2}', 0, 2, '2019-04-19 14:58:03', '2019-04-19 14:58:03');
INSERT INTO `tbl_dt_juge_duplicate` VALUES (8, 14, '{\"balance\":13116.35,\"id\":2}', 0, 2, '2019-04-19 15:01:51', '2019-04-19 15:01:51');
INSERT INTO `tbl_dt_juge_duplicate` VALUES (9, 15, '{\"balance\":13116.35,\"id\":2}', 0, 2, '2019-04-19 15:05:52', '2019-04-19 15:05:52');
INSERT INTO `tbl_dt_juge_duplicate` VALUES (10, 16, '{\"balance\":13116.35,\"id\":2}', 0, 2, '2019-04-19 15:08:56', '2019-04-19 15:09:29');

SET FOREIGN_KEY_CHECKS = 1;
