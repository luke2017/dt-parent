/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50724
 Source Host           : localhost:3306
 Source Schema         : dt_stock

 Target Server Type    : MySQL
 Target Server Version : 50724
 File Encoding         : 65001

 Date: 19/04/2019 16:31:35
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tbl_dt_msg
-- ----------------------------
DROP TABLE IF EXISTS `tbl_dt_msg`;
CREATE TABLE `tbl_dt_msg`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '消息名称',
  `business_val` int(11) NOT NULL COMMENT '业务值（接收服务通过该值知道消息是哪个业务处理）',
  `queue_val` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '发送到哪个消息队列',
  `data` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '消息的内容（JSON）',
  `status_val` int(11) NOT NULL DEFAULT 1 COMMENT '状态值（1：预发送 2：发送成功 3：发送失败）',
  `retry_num` int(11) NOT NULL COMMENT '重试发送次数（最多5次）',
  `field1` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '预留字段1',
  `field2` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '预留字段2',
  `field3` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '预留字段3',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_time` datetime(0) DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tbl_dt_msg
-- ----------------------------
INSERT INTO `tbl_dt_msg` VALUES (1, '扣减用户余额', 1001, 'dt_queue_user', '{\"balance\":144.0,\"id\":1}', 2, 0, NULL, NULL, NULL, '2019-04-18 15:04:07', '2019-04-18 17:16:56');
INSERT INTO `tbl_dt_msg` VALUES (2, '扣减用户余额', 1001, 'dt_queue_user', '{\"balance\":144.0,\"id\":1}', 2, 0, NULL, NULL, NULL, '2019-04-18 15:07:13', '2019-04-18 17:17:04');
INSERT INTO `tbl_dt_msg` VALUES (3, '扣减用户余额', 1001, 'dt_queue_user', '{\"balance\":72.0,\"id\":1}', 2, 0, NULL, NULL, NULL, '2019-04-18 15:08:16', '2019-04-18 17:17:04');
INSERT INTO `tbl_dt_msg` VALUES (4, '扣减用户余额', 1001, 'dt_queue_user', '{\"balance\":7.2,\"id\":1}', 2, 0, NULL, NULL, NULL, '2019-04-19 10:59:32', '2019-04-19 10:59:55');
INSERT INTO `tbl_dt_msg` VALUES (5, '扣减用户余额', 1001, 'dt_queue_user', '{\"balance\":7.2,\"id\":1}', 2, 0, NULL, NULL, NULL, '2019-04-19 11:14:53', '2019-04-19 11:16:37');
INSERT INTO `tbl_dt_msg` VALUES (6, '扣减用户余额', 1001, 'dt_queue_user', '{\"balance\":7.2,\"id\":1}', 2, 0, NULL, NULL, NULL, '2019-04-19 11:22:23', '2019-04-19 11:23:31');
INSERT INTO `tbl_dt_msg` VALUES (7, '扣减用户余额', 1001, 'dt_queue_user', '{\"balance\":7.2,\"id\":1}', 2, 0, NULL, NULL, NULL, '2019-04-19 11:31:39', '2019-04-19 11:32:33');
INSERT INTO `tbl_dt_msg` VALUES (8, '扣减用户余额', 1001, 'dt_queue_user', '{\"balance\":7.2,\"id\":1}', 2, 0, NULL, NULL, NULL, '2019-04-19 14:25:09', '2019-04-19 14:25:30');
INSERT INTO `tbl_dt_msg` VALUES (9, '扣减用户余额', 1001, 'dt_queue_user', '{\"balance\":13116.35,\"id\":2}', 2, 0, NULL, NULL, NULL, '2019-04-19 14:34:21', '2019-04-19 14:34:30');
INSERT INTO `tbl_dt_msg` VALUES (10, '扣减用户余额', 1001, 'dt_queue_user', '{\"balance\":13116.35,\"id\":2}', 2, 1, NULL, NULL, NULL, '2019-04-19 14:39:53', '2019-04-19 14:41:30');
INSERT INTO `tbl_dt_msg` VALUES (11, '扣减用户余额', 1001, 'dt_queue_user', '{\"balance\":13116.35,\"id\":2}', 2, 0, NULL, NULL, NULL, '2019-04-19 14:45:19', '2019-04-19 14:46:13');
INSERT INTO `tbl_dt_msg` VALUES (12, '扣减用户余额', 1001, 'dt_queue_user', '{\"balance\":13116.35,\"id\":2}', 2, 0, NULL, NULL, NULL, '2019-04-19 14:50:47', '2019-04-19 14:51:38');
INSERT INTO `tbl_dt_msg` VALUES (13, '扣减用户余额', 1001, 'dt_queue_user', '{\"balance\":13116.35,\"id\":2}', 2, 0, NULL, NULL, NULL, '2019-04-19 14:54:44', '2019-04-19 14:55:38');
INSERT INTO `tbl_dt_msg` VALUES (14, '扣减用户余额', 1001, 'dt_queue_user', '{\"balance\":13116.35,\"id\":2}', 2, 0, NULL, NULL, NULL, '2019-04-19 15:01:23', '2019-04-19 15:01:38');
INSERT INTO `tbl_dt_msg` VALUES (15, '扣减用户余额', 1001, 'dt_queue_user', '{\"balance\":13116.35,\"id\":2}', 2, 0, NULL, NULL, NULL, '2019-04-19 15:05:15', '2019-04-19 15:05:38');
INSERT INTO `tbl_dt_msg` VALUES (16, '扣减用户余额', 1001, 'dt_queue_user', '{\"balance\":13116.35,\"id\":2}', 2, 0, NULL, NULL, NULL, '2019-04-19 15:07:47', '2019-04-19 15:08:38');

SET FOREIGN_KEY_CHECKS = 1;
