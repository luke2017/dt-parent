package com.luke.dt.zuul.hystrix;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class HystrixCommandController {

    @GetMapping("/hystrixTimeout")
    public String hystrixTimeout(@RequestParam String service){
        log.info("{} hystrixTimeout",service);
        return service+" service hystrixTimeout";
    }

    /*@HystrixCommand(commandKey = "authHystrixCommand")
    public void authHystrixCommand(){
        log.info("authHystrixCommand");
    }*/

}
