package com.luke.dt.zuul;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * 这里虽然用来zuul名字，但是实际使用的
 * 是springcloud-gateway实现网关
 */
@SpringBootApplication
@EnableEurekaClient
public class ZuulApp {

    public static void main(String[] args) {
        SpringApplication.run(ZuulApp.class,args);
    }

}
