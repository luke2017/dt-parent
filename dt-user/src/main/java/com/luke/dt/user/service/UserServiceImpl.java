package com.luke.dt.user.service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.luke.dt.commons.entitys.user.TblUser;
import com.luke.dt.commons.exception.ServiceException;
import com.luke.dt.commons.input.user.FindUserReq;
import com.luke.dt.commons.inter.stock.StockInterController;
import com.luke.dt.commons.page.PageBaseInfo;
import com.luke.dt.user.dao.TblUserMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import javax.annotation.Resource;

@Slf4j
@Service
@Transactional(readOnly = true)
public class UserServiceImpl implements UserService{

    @Resource
    private TblUserMapper tblUserMapper;

    @Autowired(required = false)
    private StockInterController stockInterController;

    @Override
    public Page<TblUser> findByPage(PageBaseInfo pageBaseInfo) throws ServiceException {
        PageHelper.startPage(pageBaseInfo.getPageNum(),pageBaseInfo.getPageSize());
        return tblUserMapper.findByPage();
    }

    @Override
    public TblUser findUser(FindUserReq findUserReq) throws ServiceException {
        TblUser paramUsere = new TblUser();
        paramUsere.setName(findUserReq.getName());
        return tblUserMapper.selectOne(paramUsere);
    }

}
