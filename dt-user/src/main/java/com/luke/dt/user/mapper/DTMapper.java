package com.luke.dt.user.mapper;

import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

/**
 * 自定义Mapper
 * @param <T>
 */
public interface DTMapper<T> extends Mapper<T>, MySqlMapper<T> {

}
