package com.luke.dt.user.dao;

import com.luke.dt.commons.entitys.shared.TblDtJudeDuplicate;
import com.luke.dt.user.mapper.DTMapper;

public interface TblDtJudeDuplicateMapper extends DTMapper<TblDtJudeDuplicate> {

}
