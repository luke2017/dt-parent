package com.luke.dt.user.controller;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.luke.dt.commons.entitys.stock.TblStock;
import com.luke.dt.commons.entitys.user.TblUser;
import com.luke.dt.commons.enums.ErrorCodeEnums;
import com.luke.dt.commons.exception.ServiceException;
import com.luke.dt.commons.input.user.FindUserReq;
import com.luke.dt.commons.output.Result;
import com.luke.dt.commons.page.PageBaseInfo;
import com.luke.dt.user.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import java.util.List;

/**
 * 用户控制类
 */
@Slf4j
@RestController
@RequestMapping("/dt-user/user")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/findByPage")
    public Page<TblUser> findByPage(@RequestParam(required = true) Integer pageNum,
                                    @RequestParam(required = true) Integer pageSize){
        log.info("findByPage params:pageNum={},pageSize={}",pageNum,pageSize);
        PageBaseInfo pageBaseInfo = new PageBaseInfo();
        pageBaseInfo.setPageNum(pageNum);
        pageBaseInfo.setPageSize(pageSize);
        try {
            return userService.findByPage(pageBaseInfo);
        } catch (ServiceException e) {
            log.error(e.getMessage());
        }
        return null;
    }

    /**
     * 查询用户
     * @param findUserReq
     * @param bindingResult
     * @return
     */
    @GetMapping(value = "/findUser")
    public Result findUser(@Validated FindUserReq findUserReq, BindingResult bindingResult){
        try {
            log.info("findUser params:findUserReq={}",findUserReq);
            //参数校验
            if(bindingResult.hasErrors()){
                List<ObjectError> allErrors = bindingResult.getAllErrors();
                StringBuffer sb = new StringBuffer();
                for(ObjectError error : allErrors){
                    sb.append(error.getDefaultMessage()+",");
                }
                String message = sb.substring(0,sb.length()-1);
                return new Result(ErrorCodeEnums.ERROR_CODE_INVALID_PARAMS.getCode(),
                        ErrorCodeEnums.ERROR_CODE_INVALID_PARAMS.getMessage(),message);
            }
            //调用service
            TblUser dbUser = userService.findUser(findUserReq);
            return new Result(ErrorCodeEnums.ERROR_CODE_SUCCESS.getCode(),
                    ErrorCodeEnums.ERROR_CODE_SUCCESS.getMessage(),JSONObject.toJSON(dbUser));
        } catch (ServiceException e) {
            log.error("业务异常：error:{}",e.getMessage());
            return new Result(e.getCode(),e.getMessage());
        }catch (Exception e) {
            log.error("系统异常：error:{}",e.getMessage());
            return new Result(ErrorCodeEnums.ERROR_CODE_ERROR_SERVER.getCode(),
                    e.getMessage());
        }

    }



}
