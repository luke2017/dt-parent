package com.luke.dt.user.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 网关测试类
 */
@Slf4j
@RestController
@RequestMapping("/dt-user/geteway")
public class GatewayTestController {

    @GetMapping("/testGateway")
    public String testGateway(){
        return "testGateway";
    }

}
