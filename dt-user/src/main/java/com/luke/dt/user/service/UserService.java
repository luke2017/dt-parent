package com.luke.dt.user.service;

import com.github.pagehelper.Page;
import com.luke.dt.commons.entitys.user.TblUser;
import com.luke.dt.commons.exception.ServiceException;
import com.luke.dt.commons.input.user.FindUserReq;
import com.luke.dt.commons.page.PageBaseInfo;

public interface UserService {

    Page<TblUser> findByPage(PageBaseInfo pageBaseInfo) throws ServiceException;

    TblUser findUser(FindUserReq findUserReq) throws ServiceException;

}
