package com.luke.dt.user.service;

import com.luke.dt.commons.entitys.shared.TblDtJudeDuplicate;
import com.luke.dt.commons.exception.ServiceException;

public interface JudeDuplicateService {

    TblDtJudeDuplicate handleReduUserBalance(String msg) throws ServiceException;

    Integer  handleMsgFail(String msg) throws ServiceException;

}
