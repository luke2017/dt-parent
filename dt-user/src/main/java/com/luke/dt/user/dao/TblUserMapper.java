package com.luke.dt.user.dao;

import com.github.pagehelper.Page;
import com.luke.dt.commons.entitys.user.TblUser;
import com.luke.dt.user.mapper.DTMapper;
import org.apache.ibatis.annotations.Param;

public interface TblUserMapper extends DTMapper<TblUser> {

    Page<TblUser> findByPage();

    Integer updateUserBalance(@Param("id") Long userId, @Param("reduBalance") Float reduBalance);

}
