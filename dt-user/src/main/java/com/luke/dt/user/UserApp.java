package com.luke.dt.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import tk.mybatis.spring.annotation.MapperScan;

/**
 * 注意@MapperScan导入的是tk.mybatis的
 * @EnableTransactionManagement 开启事务注解
 *
 * @EnableFeignClients需要将basePackages
 * 指向dt-commons项目包，这里存放了各个供服务间调用的FeignClient
 *
 * @EnableFeignClients basePackages不要指向自己服务本身的接口，否则报错
 *
 * 这里还需要注意一点（可能性极低极低）：rabbitmq在极端情况下会发生消息丢失
 * 发送者A将消息发送给rabbitmq并且拿到了ack，此时A会将消息表的消息置为消息发送成功。
 * 当时消息队列此时的持久化消息只是缓存到了操作系统的缓冲区（延迟了一会才存文件），挂了。那么该消息将
 * 无法进入rabbitmq持久化文件。这种情况下B服务将永远拿不到A发送过来的这个消息。
 * 针对这种可能性极低极低的情况，建议使用一个同步微服务去比较各个服务，A消息表标记发送了的数据
 * 是否在B表存在，如果不存在则可能是消息队列还没有投送，或者就是发生了上述情况。
 *
 */
@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients(basePackages = { "com.luke.dt.commons.inter.order",
        "com.luke.dt.commons.inter.stock" })
@EnableTransactionManagement
@MapperScan(basePackages = { "com.luke.dt.user.dao" })
public class UserApp {

    public static void main(String[] args) {
        SpringApplication.run(UserApp.class,args);
    }

}
