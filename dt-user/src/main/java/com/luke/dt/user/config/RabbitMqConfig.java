package com.luke.dt.user.config;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 这里是创建了队列，生产环境尽量不要使用这种方式
 * 在rabbitmq管理台配置消息队列
 */
@Configuration
public class RabbitMqConfig {

    @Bean
    public Queue helloQueue(){
        return new Queue("dt_queue_user");
    }

}
