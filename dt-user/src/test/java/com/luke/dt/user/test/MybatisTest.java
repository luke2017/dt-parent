package com.luke.dt.user.test;

import com.luke.dt.commons.entitys.shared.TblDtJudeDuplicate;
import com.luke.dt.commons.entitys.user.TblUser;
import com.luke.dt.user.dao.TblDtJudeDuplicateMapper;
import com.luke.dt.user.dao.TblUserMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import javax.annotation.Resource;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MybatisTest {

    @Resource
    private TblUserMapper tblUserMapper;

    @Resource
    private TblDtJudeDuplicateMapper tblDtJudeDuplicateMapper;

    @Test
    public void testSelectOne(){
        TblUser paramBean = new TblUser();
        paramBean.setId(1L);
        TblUser dbUser = tblUserMapper.selectOne(paramBean);
        System.out.println("dbUser:"+dbUser);
    }

    @Test
    public void testSelectOne2(){
        TblDtJudeDuplicate findJudeDuplicate = new TblDtJudeDuplicate();
        findJudeDuplicate.setMsgId(7L);
        TblDtJudeDuplicate dbJudeDuplicate = tblDtJudeDuplicateMapper.selectOne(findJudeDuplicate);
        System.out.println(dbJudeDuplicate);
    }

}
