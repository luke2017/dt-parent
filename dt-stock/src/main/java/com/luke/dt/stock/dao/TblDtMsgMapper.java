package com.luke.dt.stock.dao;

import com.luke.dt.commons.entitys.shared.TblDtMSG;
import com.luke.dt.stock.mapper.DTMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TblDtMsgMapper extends DTMapper<TblDtMSG> {

    /**
     * 查询还未有发送成功，而且重试次数没有达到最大值的记录
     * @param statusVal
     * @param retryMaxNum
     * @return
     */
    public List<TblDtMSG> findDtMSGList(@Param("statusVal") Integer statusVal, @Param("retryMaxNum") Integer retryMaxNum);

}
