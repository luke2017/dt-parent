package com.luke.dt.stock.service;

import com.luke.dt.commons.exception.ServiceException;

public interface AmqpSenderService {

    void handlesendDtMSG() throws ServiceException;

}
