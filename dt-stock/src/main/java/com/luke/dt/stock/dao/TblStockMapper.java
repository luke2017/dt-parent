package com.luke.dt.stock.dao;

import com.github.pagehelper.Page;
import com.luke.dt.commons.entitys.stock.TblStock;
import com.luke.dt.stock.mapper.DTMapper;
import org.apache.ibatis.annotations.Param;

public interface TblStockMapper extends DTMapper<TblStock> {

    Page<TblStock> findByPage();

    /**
     * 扣减指定商品的库存量
     * @param id
     * @param deStock
     */
    Integer updateStock(@Param("id") Long id, @Param("deStock") Integer deStock);

}
