package com.luke.dt.stock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import tk.mybatis.spring.annotation.MapperScan;

/**
 *实践分布式事物（基于消息队列的最终一致性（发送端未采用发送ack，采用轮询。也可以采用ack））
 * 接收端需要解决防止重复消费问题（幂等性）
 * 参考：https://mp.weixin.qq.com/s/7u5zfrLzk38tDwOfDEkuqw
 */
@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients(basePackages = { "com.luke.dt.commons.inter.order",
        "com.luke.dt.commons.inter.user" })
@EnableTransactionManagement
@MapperScan(basePackages = { "com.luke.dt.stock.dao" })
@EnableScheduling //开启定时任务
public class StockApp {

    public static void main(String[] args) {
        SpringApplication.run(StockApp.class,args);
    }

}
