package com.luke.dt.stock.schedule;

import com.luke.dt.stock.service.AmqpSenderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 分布式事物后台任务
 * 将消息表数据发送到对应的消息队列
 */
@Slf4j
@Component
public class DtScheduleTask {

    @Autowired
    private AmqpSenderService amqpSenderService;

    @Scheduled(fixedRate = 60*1000)
    public void sendDtMSG(){
        try {
            log.info("=======sendDtMSG开始执行=======");
            amqpSenderService.handlesendDtMSG();
            log.info("=======sendDtMSG执行完毕=======");
        } catch (Exception e) {
            log.error("sendDtMSG出现错误：{}",e.getMessage());
        }
    }

}
