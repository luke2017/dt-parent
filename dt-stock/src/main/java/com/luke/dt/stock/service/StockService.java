package com.luke.dt.stock.service;

import com.luke.dt.commons.entitys.stock.TblStock;
import com.luke.dt.commons.exception.ServiceException;
import com.luke.dt.commons.input.stock.BuyGoodsReqDto;
import com.luke.dt.commons.output.Result;

public interface StockService {

    TblStock findStock(String merchan) throws ServiceException;

    Result buyGoods(BuyGoodsReqDto buyGoodsReqDto) throws ServiceException;

}
