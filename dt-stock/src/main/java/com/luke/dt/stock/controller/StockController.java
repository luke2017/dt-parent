package com.luke.dt.stock.controller;

import com.luke.dt.commons.entitys.stock.TblStock;
import com.luke.dt.commons.enums.ErrorCodeEnums;
import com.luke.dt.commons.exception.ServiceException;
import com.luke.dt.commons.input.stock.BuyGoodsReqDto;
import com.luke.dt.commons.input.stock.FindStockReq;
import com.luke.dt.commons.output.Result;
import com.luke.dt.stock.service.StockService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/dt-stock/stock")
public class StockController {

    @Autowired
    private StockService stockService;

    @GetMapping(value = "/findStock")
    public Result findStock(@Validated FindStockReq findStockReq, BindingResult bindingResult){
        try {
            log.info("findStock params:findStockReq={}",findStockReq);
            //参数校验
            if(bindingResult.hasErrors()){
                List<ObjectError> allErrors = bindingResult.getAllErrors();
                StringBuffer sb = new StringBuffer();
                for(ObjectError error : allErrors){
                    sb.append(error.getDefaultMessage()+",");
                }
                String message = sb.substring(0,sb.length()-1);
                return new Result(ErrorCodeEnums.ERROR_CODE_INVALID_PARAMS.getCode(),
                        ErrorCodeEnums.ERROR_CODE_INVALID_PARAMS.getMessage(),message);
            }
            //调用service
            TblStock dbStock = stockService.findStock(findStockReq.getMerchan());
            return new Result(ErrorCodeEnums.ERROR_CODE_SUCCESS.getCode(),
                    ErrorCodeEnums.ERROR_CODE_SUCCESS.getMessage(),dbStock);
        } catch (ServiceException e) {
            log.error("业务异常：error:{}",e.getMessage());
            return new Result(e.getCode(),e.getMessage());
        }catch (Exception e) {
            log.error("系统异常：error:{}",e.getMessage());
            return new Result(ErrorCodeEnums.ERROR_CODE_ERROR_SERVER.getCode(),
                    e.getMessage());
        }

    }

    /**
     * 购买商品
     * @param buyGoodsReqDto
     * @return
     */
    @PostMapping("/buyGoods")
    public Result buyGoods(@RequestBody @Validated BuyGoodsReqDto buyGoodsReqDto){
        try {
            log.info("buyGoods params:buyGoodsReqDto={}",buyGoodsReqDto);
            return stockService.buyGoods(buyGoodsReqDto);
        } catch (ServiceException e) {
            log.error("业务异常：error:{}",e.getMessage());
            return new Result(e.getCode(),e.getMessage());
        }catch (Exception e) {
            log.error("系统异常：error:{}",e.getMessage());
            return new Result(ErrorCodeEnums.ERROR_CODE_ERROR_SERVER.getCode(),
                    e.getMessage());
        }
    }

}
