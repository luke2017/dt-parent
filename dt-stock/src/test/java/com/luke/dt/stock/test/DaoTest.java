package com.luke.dt.stock.test;

import com.luke.dt.commons.entitys.shared.TblDtMSG;
import com.luke.dt.commons.entitys.stock.TblStock;
import com.luke.dt.stock.dao.TblDtMsgMapper;
import com.luke.dt.stock.dao.TblStockMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import javax.annotation.Resource;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DaoTest {

    @Resource
   private TblStockMapper tblStockMapper;

    @Resource
    private TblDtMsgMapper tblDtMsgMapper;

    @Test
    public void testUpdateStock(){
        Integer result = tblStockMapper.updateStock(3L, 2);
        System.out.println("result:"+result);
    }

    @Test
    public void testFindStock(){
        List<TblStock> list = tblStockMapper.selectAll();
        System.out.println("list size"+list.size());
    }

    @Test
    public void testFindDtMSGList(){
        List<TblDtMSG> dtMSGList = tblDtMsgMapper.findDtMSGList(2, 5);
        System.out.println(dtMSGList.size());
    }

}
